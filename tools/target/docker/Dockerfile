######################################################################
#  _   _ ____  _ _ _           
# | \ | / ___|(_) (_) ___ ___  
# |  \| \___ \| | | |/ __/ _ \ 
# | |\  |___) | | | | (_| (_) |
# |_| \_|____/|_|_|_|\___\___/ 
#
# Simplicity MetaPlat Tools Image                             
#
# This file instructs Docker to build an image containing bioinformatic tools required by the MetaPlat service.
#

######################################################################
FROM		nsilico/metaplat-base:latest
MAINTAINER 	NSilico Bioinformatics <simplicity@nsilico.com>
LABEL		description="Simplicity MetaPlat Tools" version="1.0" Vendor="NSilico Ltd."
ENV		FARM_USER	simplicity
######################################################################


USER	root
WORKDIR	/root

# Simplicity Pipeline components
# Add Farm Server user

RUN	useradd -m -s /bin/bash ${FARM_USER}

# Make ssh dir
RUN mkdir /root/.ssh/

# Install Tools
# Create folders that are filled later...
RUN mkdir /home/${FARM_USER}/bin

RUN echo 'options(\n\
  repos = c(CRAN = "https://cran.r-project.org/"),\n\
  download.file.method = "libcurl",\n\
  # Detect number of physical cores\n\
  Ncpus = parallel::detectCores(logical=FALSE)\n\
)' >> /etc/R/Rprofile.site
RUN Rscript -e "install.packages('stringr')"
RUN Rscript -e "install.packages('ggfortify')"
RUN Rscript -e "install.packages('ggplot2')"

#Install biopython
RUN pip install biopython

#Install Metaphlan2
RUN wget https://bitbucket.org/biobakery/metaphlan2/get/default.zip && \
	unzip default.zip -d /opt/
RUN python /opt/biobakery-metaphlan2-*/metaphlan2.py --input_type fastq
RUN mv /opt/biobakery-metaphlan2-* /opt/biobakery-metaphlan2

#Install FastQC and BBmap
RUN wget -q -O fastqc.zip http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.5.zip && \
    unzip fastqc.zip -d /opt/ && \
    chmod 755 /opt/FastQC/fastqc && \
    rm fastqc.zip

RUN wget -q -O BBMap_37.09.tar.gz https://sourceforge.net/projects/bbmap/files/BBMap_37.09.tar.gz && \
	tar -xzf BBMap_37.09.tar.gz && \
	mv bbmap/ /opt/ && \
	rm BBMap_37.09.tar.gz

RUN curl -fsSL get.nextflow.io | bash && \
	mv nextflow /opt/
RUN chmod +rwx /opt/nextflow
RUN apt install python3-tk --yes
RUN apt install  python3-pip --yes

RUN pip3 install pandas --upgrade
RUN apt install pkg-config

#Clonning Scripts
RUN git clone https://github.com/brunoand/NMP.git && \
        chmod +x NMP/Scripts/Immunoadept_barchart.py && \
	echo e && \
        cp NMP/Scripts/Immunoadept_barchart.py /opt/
		
ENV PATH $PATH:/opt/::/opt/FastQC/:/opt/bbmap

RUN Rscript -e "install.packages('jsonlite')"

RUN	echo "export PATH=\$PATH:/home/${FARM_USER}/bin" >> /home/${FARM_USER}/.bashrc