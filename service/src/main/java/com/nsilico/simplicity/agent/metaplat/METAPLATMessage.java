package com.nsilico.simplicity.agent.metaplat;

import com.nsilico.simplicity.queue.common.SimplicityMessage;
import lombok.Data;

@Data
public class METAPLATMessage extends SimplicityMessage {

    private Payload payload;

    @Data
    public static class Payload {
    }
}
