package com.nsilico.simplicity.agent.metaplat;

import com.nsilico.simplicity.queue.client.MessageQueueRESTClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class METAPLATMessageQueueRESTClient extends MessageQueueRESTClient<METAPLATMessage> {
    public METAPLATMessageQueueRESTClient(@Value("${queue.name:metaplat_messages_in}") String queueName,
                                     @Value("${queue.username}") String username,
                                     @Value("${queue.password}") String password) {
        super(queueName, username, password);
    }
}
