package com.nsilico.simplicity.agent.metaplat;

import com.google.gson.Gson;
import com.nsilico.simplicity.agent.MessageProcessor;
import com.nsilico.simplicity.agent.Progress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class METAPLATMessageProcessor extends MessageProcessor<METAPLATMessage> {

    @Value("${simplicity.blast.executable:./services/NMP.nf}")
    String executable;

    @Value("${local.working.directory:/tmp/nmp}")
    private String localWorkingDirectory;

    @Autowired
    Gson gson;

    @Override
    protected ProcessBuilder createProcessBuilder(METAPLATMessage message) {
        return new ProcessBuilder("/home/simplicity/services/run.sh");
    }

    @Override
    protected Progress getCurrentProgress(Integer returnValue) {
        return Progress
                .builder()
                .state(Progress.State.IN_PROGRESS)
                .completion(0.5) //Until we find some way of measuring actual blast progress
                .build();
    }
}
