package com.nsilico.simplicity.agent.metaplat;

import com.nsilico.simplicity.agent.QueuePoller;
import com.nsilico.simplicity.agent.metaplat.METAPLATMessage;
import org.springframework.stereotype.Component;

@Component
public class METAPLATQueuePoller extends QueuePoller<METAPLATMessage> {
    @Override
    protected METAPLATMessage getNextMessage() {
        return client.getNextMessage(METAPLATMessage.class);
    }

    @Override
    protected void markAsProcessed(String messageId) {
        client.markAsProcessed(messageId, METAPLATMessage.class);
    }
}
