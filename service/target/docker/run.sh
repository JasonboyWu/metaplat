#!/usr/bin/env bash
cd services
mkdir -p /home/simplicity/work/output
mkdir -p /home/simplicity/work/NMPoutput
#----------------------------------------------------------------------------------------
echo $(jq -r .pipelineId /home/simplicity/work/input/metaplat.json)
userMRN=$(jq -r .payload.subjectMRN /home/simplicity/work/input/metaplat.json)
echo 'User MRN is: '$userMRN
submitTime=$(jq -r .payload.subjectDoB /home/simplicity/work/input/metaplat.json)
echo 'Submit Time is: '$submitTime
dataUploadType=$(jq -r .payload.dataUploadType /home/simplicity/work/input/metaplat.json)
echo 'Data Upload Type is: '$dataUploadType
readType=$(jq -r .payload.readType /home/simplicity/work/input/metaplat.json)
echo 'Read Type is: '$readType
#----------------------------------------------------------------------------------------
if [ $dataUploadType == 'file' ]
then
	if [ $readType == 'single-end' ]
	then
		nextflow run -c nextflow.config NMP.nf --reads_R1 /home/simplicity/work/input/forward.fastq --date $submitTime --prefix $userMRN --library $readType --reference ./Reference/ --outdir /home/simplicity/work/NMPoutput 2>&1 | tee /home/simplicity/work/output/nmp.log
	else
		nextflow run -c nextflow.config NMP.nf --reads_R1 /home/simplicity/work/input/forward.fastq --reads_R2 /home/simplicity/work/input/reverse.fastq --date $submitTime --prefix $userMRN --reference ./Reference/ --outdir /home/simplicity/work/NMPoutput 2>&1 | tee /home/simplicity/work/output/nmp.log
	fi
else
	echo 'Using Public URL'
	mkdir -p /home/simplicity/work/filedownload
	forwardFileUrl=$(jq -r .payload.forwardFileUrl /home/simplicity/work/input/metaplat.json)
	echo 'ForwardFileUrl is: '$forwardFileUrl
	echo '----------------------------------------------------------------------------------------'
	first=$(echo $forwardFileUrl | rev | cut -d '.' --complement -f1 | rev)
	echo 'First Part is :'$first
	echo '----------------------------------------------------------------------------------------'
	second=$(echo $forwardFileUrl | rev | cut -d '.' -f 1 | rev)
	echo 'Second Part is :'$second
	echo '----------------------------------------------------------------------------------------'
	format=$(echo $second | cut -b 1-2)
	echo 'Final Second Part is :'$format
	echo '----------------------------------------------------------------------------------------'
	reverseFileUrl=$(jq -r .payload.reverseFileUrl /home/simplicity/work/input/metaplat.json)
	echo 'ReverseFileUrl is: '$reverseFileUrl
	echo '----------------------------------------------------------------------------------------'
	firstR=$(echo $reverseFileUrl | rev | cut -d '.' --complement -f1 | rev)
	echo 'FirstR Part is :'$firstR
	echo '----------------------------------------------------------------------------------------'
	secondR=$(echo $reverseFileUrl | rev | cut -d '.' -f 1 | rev)
	echo 'SecondR Part is :'$secondR
	echo '----------------------------------------------------------------------------------------'
	formatR=$(echo $secondR | cut -b 1-2)
	echo 'Final SecondR Part is :'$formatR
	echo '----------------------------------------------------------------------------------------'
	if [ $readType == 'single-end' ]
	then
		if [ $format == 'gz' ]
		then
			finalurl="$first.$format"
			wget -P /home/simplicity/work/filedownload/ $finalurl
			gunzip /home/simplicity/work/filedownload/*.gz
			mv /home/simplicity/work/filedownload/*.fastq /home/simplicity/work/input/*.fastq
			nextflow run -c nextflow.config NMP.nf --reads_R1 /home/simplicity/work/input/*.fastq --date $submitTime --prefix $userMRN --library $readType --reference ./Reference/ --outdir /home/simplicity/work/NMPoutput 2>&1 | tee /home/simplicity/work/output/nmp.log
		else
			secondf=$(echo $second | cut -b 1-5)
			echo 'Final Second Part is :'$secondf
			forwardURL="$first.$secondf"
			echo 'FinalF URL is :'$forwardURL
			wget -P /home/simplicity/work/input/ $forwardURL
			nextflow run -c nextflow.config NMP.nf --reads_R1 /home/simplicity/work/input/*.fastq --date $submitTime --prefix $userMRN --library $readType --reference ./Reference/ --outdir /home/simplicity/work/NMPoutput 2>&1 | tee /home/simplicity/work/output/nmp.log
		fi
	else
		if [ $format == 'gz' ]
		then
			finalurl="$first.$format"
			wget -P /home/simplicity/work/filedownload/ $finalurl
			gunzip /home/simplicity/work/filedownload/*.gz
			mv /home/simplicity/work/filedownload/*.fastq /home/simplicity/work/input/forward.fastq
			if [ $formatR == 'gz' ]
			then
				finalRurl="$firstR.$formatR"
				wget -P /home/simplicity/work/filedownload/ $finalRurl
				gunzip /home/simplicity/work/filedownload/*.gz
				mv /home/simplicity/work/filedownload/*.fastq /home/simplicity/work/input/reverse.fastq
			else
				secondfR=$(echo $secondR | cut -b 1-5)
				echo 'Final SecondR Part is :'$secondfR
				reverseURL="$firstR.$secondfR"
				echo 'Final URL is :'$reverseURL
				wget -P /home/simplicity/work/filedownload/ $reverseURL
				mv /home/simplicity/work/filedownload/*.fastq /home/simplicity/work/input/reverse.fastq
			fi
		else
			secondf=$(echo $second | cut -b 1-5)
			echo 'Final Second Part is :'$secondf
			forwardURL="$first.$secondf"
			echo 'Final URL is :'$forwardURL
			wget -P /home/simplicity/work/filedownload/ $forwardURL
			mv /home/simplicity/work/filedownload/*.fastq /home/simplicity/work/input/forward.fastq
			if [ $secondR == 'gz' ]
			then
				finalRurl="$firstR.$formatR"
				wget -P /home/simplicity/work/filedownload/ $finalRurl
				gunzip /home/simplicity/work/filedownload/*.gz
				mv /home/simplicity/work/filedownload/*.fastq /home/simplicity/work/input/reverse.fastq
			else
				secondfR=$(echo $secondR | cut -b 1-5)
				echo 'Final SecondR Part is :'$secondfR
				reverseURL="$firstR.$secondfR"
				echo 'Final URL is :'$reverseURL
				wget -P /home/simplicity/work/filedownload/ $reverseURL
				mv /home/simplicity/work/filedownload/*.fastq /home/simplicity/work/input/reverse.fastq
			fi
		fi
		nextflow run -c nextflow.config NMP.nf --reads_R1 /home/simplicity/work/input/forward.fastq --reads_R2 /home/simplicity/work/input/reverse.fastq --date $submitTime --prefix $userMRN --reference ./Reference/ --outdir /home/simplicity/work/NMPoutput 2>&1 | tee /home/simplicity/work/output/nmp.log
	fi
	rm -R /home/simplicity/work/filedownload
fi
Rscript ./Scripts/Immunoadept_Barchart_Metagenomics.R $userMRN /home/simplicity/work/NMPoutput/$userMRN/Matrix/${submitTime}.txt 2>&1 | tee /home/simplicity/work/output/chat.log
cp /home/simplicity/work/NMPoutput/$userMRN/Matrix/* /home/simplicity/work/output
cp /home/simplicity/work/NMPoutput/$userMRN/QC/* /home/simplicity/work/output
rm -R /home/simplicity/work/NMPoutput
rm -R /home/simplicity/services/work/*
rm /home/simplicity/services/timeline*
rm /home/simplicity/services/trace*